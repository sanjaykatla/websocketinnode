const http = require("http")
const WebSocketServer = require("websocket").server
let connection = null;

const httpServer = http.createServer((req, res) => {
    console.log("We have received a request")
})

const websocket = new WebSocketServer({
    "httpServer": httpServer
})

websocket.on("request", request => {

    connection = request.accept(null, request.origin)
    connection.on("open", e => console.log("Opened!"))
    connection.on("close", e => console.log("Closed!"))
    connection.on("message", msg => {
        console.log(`Received msg: ${msg.data}`)
    })
})

httpServer.listen(8080, () => console.log("My server is listening on port 8080"))

